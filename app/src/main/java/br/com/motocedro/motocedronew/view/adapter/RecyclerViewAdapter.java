package br.com.motocedro.motocedronew.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.motocedro.motocedronew.R;
import br.com.motocedro.motocedronew.model.ValorMes;

/**
 * Created by loopback on 20/10/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<ValorMes> parcelasMes;

    public RecyclerViewAdapter(List<ValorMes> parcelasMes) {

        this.parcelasMes = parcelasMes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_recyclerview_precos_prazos, null);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder customViewHolder, int i) {
        ValorMes valorMes = this.parcelasMes.get(i);

        customViewHolder.parcelas.setText(valorMes.meses + " x " + valorMes.valor + " R$");

        customViewHolder.total.setText("Total " + (valorMes.meses * valorMes.valor) + " R$");
    }

    @Override
    public int getItemCount() {
        return (null != parcelasMes ? parcelasMes.size() : 0);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView parcelas;
        public TextView total;

        public ViewHolder(View itemView) {
            super(itemView);
            parcelas = (TextView) itemView.findViewById(R.id.txtParcelas);
            total = (TextView) itemView.findViewById(R.id.total);
        }
    }
}
