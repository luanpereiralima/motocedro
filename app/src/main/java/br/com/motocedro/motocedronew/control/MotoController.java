package br.com.motocedro.motocedronew.control;

import android.content.Context;
import android.util.Log;

import java.util.List;

import br.com.motocedro.motocedronew.banco.BDCor;
import br.com.motocedro.motocedronew.banco.BDMoto;
import br.com.motocedro.motocedronew.model.Cor;
import br.com.motocedro.motocedronew.model.Moto;

/**
 * Created by estilon on 26/10/15.
 */
public class MotoController {
    private BDMoto dbMoto;
    private BDCor dbCor;

    public MotoController(Context context){
        this.dbMoto = new BDMoto(context);
        this.dbCor = new BDCor(context);
    }

    public List<Moto> getMotos() {
        return dbMoto.buscar();
    }

    public void excluirMoto(Moto moto){
        dbMoto.deletar(moto);
    }

    public void alterarMoto(Moto moto){
        dbMoto.atualizar(moto);
    }

    public void adicionarMoto(Moto moto){
        dbMoto.inserir(moto);
    }

    public void adicionarCor(Cor cor){
        dbCor.inserir(cor);
    }

    public List<Cor> getCoresDeMoto(Moto moto){
        return dbCor.buscar(moto);
    }

    public void excluirCor(Cor cor){
        dbCor.deletar(cor);
    }

    public void alterarCor(Cor cor){
        dbCor.atualizar(cor);
    }
}
