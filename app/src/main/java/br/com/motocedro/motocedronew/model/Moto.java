package br.com.motocedro.motocedronew.model;


import java.io.Serializable;
import java.util.List;


/**
 * Created by estilon on 26/10/15.
 */


public class Moto implements Serializable{

    private int id;
    private String modelo;
    private float preco;

    private List<Cor> cor;

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public List<Cor> getCor() {
        return cor;
    }

    public void setCor(List<Cor> cor) {
        this.cor = cor;
    }
}
