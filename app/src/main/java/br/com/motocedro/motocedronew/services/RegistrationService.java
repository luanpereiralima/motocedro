package br.com.motocedro.motocedronew.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import java.io.IOException;
import br.com.motocedro.motocedronew.NetworkConnection.NetworkConnection;
import br.com.motocedro.motocedronew.model.GcmMotoAPI;

/**
 * Created by estilon on 07/11/15.
 */
public class RegistrationService extends IntentService {

    private static final String LOG = "LOG";

    public static final String KEY_NAME = "habemus_token";
    public static final String TOPICS_GLOBAL = "/topics/global";

    private final String SENDER = "109405882335";

    public RegistrationService() {
        super(LOG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        boolean habemusToken = preferences.getBoolean(KEY_NAME, false);

        synchronized (LOG) {

            if (!habemusToken) {

                InstanceID instanceID = InstanceID.getInstance(this);

                try {

                    String token = instanceID.getToken(SENDER, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                    preferences.edit().putBoolean(KEY_NAME, token != null && token.trim().length() > 0);

                    GcmPubSub gcmPubSub = GcmPubSub.getInstance(this);

                    gcmPubSub.subscribe(token, TOPICS_GLOBAL, null);

                    sendTokenToServer(token);



                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendTokenToServer(String token) {

        NetworkConnection networkConnection = new NetworkConnection(this);

        GcmMotoAPI gcmMotoAPI = networkConnection.create(GcmMotoAPI.class);
    }
}
