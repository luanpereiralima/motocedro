package br.com.motocedro.motocedronew.banco.constantes;

/**
 * Created by loopback on 18/07/15.
 */
public class Constantes {
    public static final String CAMPO_ID = "_id";

    //Comentarios
    public static final String
        TABELA_MOTO = "moto",
        CAMPO_MOTO_PRECO = "preco",
        CAMPO_MOTO_MODELO = "modelo";

    ///Cores
    public static final String
        TABELA_COR = "cor",
        CAMPO_COR_COR = "cor",
        CAMPO_COR_ID_MOTO = "id_moto";


}
