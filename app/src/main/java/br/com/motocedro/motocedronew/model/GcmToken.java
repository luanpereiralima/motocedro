package br.com.motocedro.motocedronew.model;

/**
 * Created by estilon on 07/11/15.
 */
public class GcmToken {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
