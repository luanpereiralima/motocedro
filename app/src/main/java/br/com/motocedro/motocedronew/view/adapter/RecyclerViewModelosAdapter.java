package br.com.motocedro.motocedronew.view.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.Serializable;
import java.util.List;

import br.com.motocedro.motocedronew.R;
import br.com.motocedro.motocedronew.model.Moto;
import br.com.motocedro.motocedronew.view.activity.ModelosActivity;
import br.com.motocedro.motocedronew.view.activity.TabsFormasPagamentoActivity;

/**
 * Created by loopback on 20/10/15.
 */
public class RecyclerViewModelosAdapter extends RecyclerView.Adapter<RecyclerViewModelosAdapter.ViewHolder> {
    private List<Moto> motos;

    public RecyclerViewModelosAdapter(List<Moto> motos) {
        this.motos = motos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_recyclerview_modelos, null);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder customViewHolder, int i) {
        final Moto itemRecycleView = this.motos.get(i);

        customViewHolder.modelo.setText(itemRecycleView.getModelo());

        customViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), TabsFormasPagamentoActivity.class);
                intent.putExtra(ModelosActivity.MOTO, (Serializable) itemRecycleView);
                v.getContext().startActivity(intent);
            }
        });

        File file = new File(customViewHolder.itemView.getContext().getFilesDir().getPath()+"/"+itemRecycleView.getModelo() + ".jpg");
        Log.d("File", file.getPath());
        Picasso.with(customViewHolder.itemView.getContext())
                .load(file).into(customViewHolder.mImageView);


    }

    @Override
    public int getItemCount() {
        return (null != motos ? motos.size() : 0);
    }




    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView modelo;

        public View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            modelo = (TextView) itemView.findViewById(R.id.txtModelo);
            mImageView = (ImageView) itemView.findViewById(R.id.imvModelo);
        }
    }
}
