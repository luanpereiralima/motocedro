package br.com.motocedro.motocedronew.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import br.com.motocedro.motocedronew.R;
import br.com.motocedro.motocedronew.model.Moto;
import br.com.motocedro.motocedronew.model.ValorMes;
import br.com.motocedro.motocedronew.view.adapter.RecyclerViewAdapter;

/**
 * Created by loopback on 07/11/15.
 */

//TODO ADAPTAR AO NOVO CALCULO
public class FinanciamentoFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String ARG_MOTO = "moto";

    public static FinanciamentoFragment newInstance(int sectionNumber, Moto moto) {
        FinanciamentoFragment fragment = new FinanciamentoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable(ARG_MOTO, moto);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Moto moto = (Moto)getArguments().getSerializable(ARG_MOTO);

        View view = inflater.inflate(R.layout.consorcio_fragment, container, false);

        RecyclerView re = (RecyclerView) view.findViewById(R.id.listaValores);

        TextView textViewValor = (TextView) view.findViewById(R.id.valor);

        textViewValor.setText(String.valueOf(moto.getPreco()));

        List<ValorMes> as = new ArrayList();

        ValorMes par = new ValorMes();
        par.meses = 12;
        par.valor = 300;
        as.add(par);
        as.add(par);
        as.add(par);
        as.add(par);
        as.add(par);

        re.setHasFixedSize(true);
        re.setAdapter(new RecyclerViewAdapter(as));
        LinearLayoutManager layout = new LinearLayoutManager(getActivity());
        re.setLayoutManager(layout);

        return view;
    }


}
