package br.com.motocedro.motocedronew.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.motocedro.motocedronew.model.Moto;
import br.com.motocedro.motocedronew.view.fragment.ConsorcioFragment;
import br.com.motocedro.motocedronew.view.fragment.FinanciamentoFragment;

/**
 * Created by loopback on 07/11/15.
 */
public class SectionsPagerFormasPagamentoAdapter extends FragmentPagerAdapter {
    private Moto moto;

    public SectionsPagerFormasPagamentoAdapter(FragmentManager fm, Moto moto) {
        super(fm);
        this.moto = moto;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).4
        switch (position) {
            case 0:
                return ConsorcioFragment.newInstance(position + 1, moto);
            case 1:
                return FinanciamentoFragment.newInstance(position + 1, moto);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Consorcio";
            case 1:
                return "Financiamento";
        }
        return null;
    }
}
