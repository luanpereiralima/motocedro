package br.com.motocedro.motocedronew.banco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.motocedro.motocedronew.banco.constantes.Constantes;
import br.com.motocedro.motocedronew.model.Cor;
import br.com.motocedro.motocedronew.model.Moto;

public class BDCor {
	private SQLiteDatabase bd;

	public BDCor(Context context){
		BDCore auxBd = new BDCore(context);
		bd = auxBd.getWritableDatabase();
	}

	public void inserir(Cor cor){
		ContentValues valores = new ContentValues();
		valores.put(Constantes.CAMPO_COR_COR, cor.getCor());
		valores.put(Constantes.CAMPO_COR_ID_MOTO, cor.getIdMoto());

        bd.insert(Constantes.TABELA_COR, null, valores);
	}
	
	public void atualizar(Cor cor){
		ContentValues valores = new ContentValues();
		valores.put(Constantes.CAMPO_COR_COR, cor.getCor());
		valores.put(Constantes.CAMPO_COR_ID_MOTO, cor.getIdMoto());
		
		bd.update(Constantes.TABELA_COR, valores, Constantes.CAMPO_ID + " = ?", new String[]{"" + Constantes.CAMPO_ID});
	}

	public void deletar(Cor cor){
		bd.delete(Constantes.TABELA_COR, Constantes.CAMPO_ID + " = " + cor.getId(), null);
	}

	public List<Cor> buscar(){
		List<Cor> list = new ArrayList<>();
		String[] colunas = new String[]{Constantes.CAMPO_ID,
                Constantes.CAMPO_COR_COR, Constantes.CAMPO_COR_ID_MOTO};
		
		Cursor cursor = bd.query(Constantes.TABELA_COR, colunas, null, null, null, null, null);

		if(cursor.getCount() > 0){
			cursor.moveToFirst();
			
			do{
				list.add(getToCursor(cursor));
			}while(cursor.moveToNext());
		}
		return list;
	}

	public List<Cor> buscar(Moto moto){
		List<Cor> list = new ArrayList<>();
		String[] colunas = new String[]{Constantes.CAMPO_ID,
				Constantes.CAMPO_COR_COR, Constantes.CAMPO_COR_ID_MOTO};

		Cursor cursor = bd.query(Constantes.TABELA_COR, colunas, Constantes.CAMPO_COR_ID_MOTO+" = "+moto.getId(), null, null, null, null);

		if(cursor.getCount() > 0){
			cursor.moveToFirst();

			do{
				list.add(getToCursor(cursor));
			}while(cursor.moveToNext());
		}

		return list;
	}

	private Cor getToCursor(Cursor cursor) {
		Cor cor = new Cor();
		cor.setId(cursor.getInt(0));
		cor.setCor(cursor.getString(1));
		cor.setIdMoto(cursor.getInt(2));
		return cor;
	}
}
