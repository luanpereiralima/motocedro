package br.com.motocedro.motocedronew.banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.motocedro.motocedronew.banco.constantes.Constantes;

public class BDCore extends SQLiteOpenHelper {
	private static final String NOME_BD = "moto_cedro";
	private static final int VERSAO_BD = 16;
	
	public BDCore(Context ctx){
		super(ctx, NOME_BD, null, VERSAO_BD);
	}
	
	
	@Override
	public void onCreate(SQLiteDatabase bd) {
		bd.execSQL("create table if not exists "+ Constantes.TABELA_MOTO+" ("+Constantes.CAMPO_ID+" INTEGER primary key AUTOINCREMENT, "+
				Constantes.CAMPO_MOTO_MODELO+" string not null, "+
				Constantes.CAMPO_MOTO_PRECO+" float not null);");

		bd.execSQL("create table if not exists "+Constantes.TABELA_COR+"("+Constantes.CAMPO_ID+" INTEGER primary key AUTOINCREMENT, "+
				Constantes.CAMPO_COR_COR+" string not null, "+
				Constantes.CAMPO_COR_ID_MOTO+" string not null);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase bd, int arg1, int arg2) {
		bd.execSQL("drop table if exists "+Constantes.TABELA_MOTO+";");
		bd.execSQL("drop table if exists "+Constantes.TABELA_COR+";");
		onCreate(bd);
	}

}
