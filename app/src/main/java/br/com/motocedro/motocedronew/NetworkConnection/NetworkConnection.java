package br.com.motocedro.motocedronew.NetworkConnection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by estilon on 07/11/15.
 */
public class NetworkConnection {

    public static final String BASE_URL = "http://10.0.2.138:3000/";

    private Context context;

    public NetworkConnection(Context context){
        this.context = context;
    }

    public <T> T create(final Class<T> service) {

        ConnectivityManager manager = (ConnectivityManager)
                this.context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        T t = null;

        if (networkInfo != null && networkInfo.isConnected()) {

            final Gson gson = new GsonBuilder().create();

            Retrofit retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            t = retrofit.create(service);
        }

        return t;
    }

}
