package br.com.motocedro.motocedronew.model;

/**
 * Created by loopback on 26/11/15.
 */
public class Cor {
    private int id;
    private String cor;
    private int idMoto;

    public String getCor() {
        return cor;
    }

    public int getId() {
        return id;
    }

    public int getIdMoto() {
        return idMoto;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdMoto(int idMoto) {
        this.idMoto = idMoto;
    }
}
