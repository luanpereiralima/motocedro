package br.com.motocedro.motocedronew.banco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;
import java.util.List;
import br.com.motocedro.motocedronew.banco.constantes.Constantes;
import br.com.motocedro.motocedronew.model.Moto;

public class BDMoto {
	private SQLiteDatabase bd;
    private Context context;

	public BDMoto(Context context){
        this.context = context;
		BDCore auxBd = new BDCore(context);
		bd = auxBd.getWritableDatabase();
	}

	public void inserir(Moto moto){
		ContentValues valores = new ContentValues();
		valores.put(Constantes.CAMPO_MOTO_MODELO, moto.getModelo());
		valores.put(Constantes.CAMPO_MOTO_PRECO, moto.getPreco());

        bd.insert(Constantes.TABELA_MOTO, null, valores);
	}

	public void atualizar(Moto moto){
		ContentValues valores = new ContentValues();
		valores.put("_id", Constantes.CAMPO_ID);
		valores.put(Constantes.CAMPO_MOTO_MODELO, moto.getModelo());
		valores.put(Constantes.CAMPO_MOTO_PRECO, moto.getPreco());
		
		bd.update(Constantes.TABELA_MOTO, valores, Constantes.CAMPO_ID + " = ?", new String[]{"" + Constantes.CAMPO_ID});
	}

	public void deletar(Moto moto){
		bd.delete(Constantes.TABELA_MOTO, Constantes.CAMPO_ID + " = " + moto.getId(), null);
	}

	public List<Moto> buscar(){
		List<Moto> list = new ArrayList<>();
		String[] colunas = new String[]{Constantes.CAMPO_ID,
                Constantes.CAMPO_MOTO_MODELO, Constantes.CAMPO_MOTO_PRECO};

        Cursor cursor = bd.query(Constantes.TABELA_MOTO, colunas, null, null, null, null, null);

///        SQLiteQueryBuilder QB = new SQLiteQueryBuilder();
   //     QB.setTables(Constantes.TABELA_MOTO+" as a, "+Constantes.TABELA_COR+" as b a."+Constantes.CAMPO_ID+" = "+Constantes.CAMPO_COR_ID_MOTO);

        if(cursor.getCount() > 0){
			cursor.moveToFirst();
			
			do{
				list.add(getToCursor(cursor));
			}while(cursor.moveToNext());
		}
		return list;
	}

    private Moto getToCursor(Cursor cursor){
        Moto moto= new Moto();
        moto.setId(cursor.getInt(0));
        moto.setModelo(cursor.getString(1));
        moto.setPreco(cursor.getFloat(2));
        moto.setCor(new BDCor(context).buscar(moto));
        return moto;
    }
}
