package com.motocedro.motocedroapp.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.motocedro.motocedroapp.NetworkConnection.NetworkConnection;
import com.motocedro.motocedroapp.model.PointAPI;

import java.util.Date;

import io.realm.Realm;

/**
 * Created by estilon on 12/11/15.
 */

public class LocationListenerService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    GoogleApiClient googleApiClient;

    LocationRequest mLocationRequest;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        googleApiClient.connect();

        return Service.START_STICKY;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        createLocationRequest();

    }

    @Override
    public void onDestroy(){
        googleApiClient.disconnect();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(0);
        mLocationRequest.setNumUpdates(1);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(10);
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.
                requestLocationUpdates(googleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        final PointRealm pointRealm = new PointRealm();

        pointRealm.setLatitude(location.getLatitude());
        pointRealm.setLongitude(location.getLongitude());
        pointRealm.setTime(new Date().getTime());

        NetworkConnection networkConnection = new NetworkConnection(getApplicationContext());

        PointAPI pointAPI = networkConnection.create(PointAPI.class);

        /*Call<PointRealm> call = pointAPI.sendPoint(pointRealm);

        call.enqueue(new Callback<PointRealm>() {
            @Override
            public void onResponse(Response<PointRealm> response, Retrofit retrofit) {
                Log.i("Upload", response.message());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });*/

        Realm realm = Realm.getInstance(getApplicationContext());

        realm.beginTransaction();

        realm.copyToRealmOrUpdate(pointRealm);

        realm.commitTransaction();

        Log.i("Location", "Location has change " + realm.allObjects(PointRealm.class).size());
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
