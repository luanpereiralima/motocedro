package br.com.motocedro.motocedronew.model.repositories;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import br.com.motocedro.motocedronew.NetworkConnection.NetworkConnection;
import br.com.motocedro.motocedronew.model.Moto;
import br.com.motocedro.motocedronew.model.MotoAPI;
import retrofit.Call;
import retrofit.Callback;

import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by estilon on 26/10/15.
 */
public class MotoRepository {

    public List<Moto> getMotos(final Context context){

        updateFromServer(context);

        List<Moto> motos = new ArrayList<Moto>();

        try {
            FileInputStream fs = context.openFileInput("motos");

            InputStreamReader isr = new InputStreamReader(fs);

            BufferedReader bufferedReader = new BufferedReader(isr);

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            Log.d("Arquivo", "terminei de ler");
            if(!sb.toString().equals("null")) {

                Gson gson = new GsonBuilder().create();

                motos = Arrays.asList(gson.fromJson(sb.toString(), Moto[].class));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return motos;

    }


    private void updateFromServer(final Context context) {


        final Gson gson = new GsonBuilder().create();

        NetworkConnection networkConnection = new NetworkConnection(context);

        MotoAPI motoAPI = networkConnection.create(MotoAPI.class);

        Call<List<Moto>> call = motoAPI.getLastModifiedMotos();

        Log.i("Log", "aqui");

        call.enqueue(new Callback<List<Moto>>() {
            @Override
            public void onResponse(Response<List<Moto>> response, Retrofit retrofit) {


                List<Moto> motos = response.body();

                Log.i("Log", motos.toString());


                try{
                    FileOutputStream fs = context.openFileOutput("motos", Context.MODE_PRIVATE);

                    fs.write(gson.toJson(motos).getBytes());

                    fs.close();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                getImageFromServer(motos, context);


            }

            public void onFailure(Throwable t) {
                Log.i("Log", "kill alberto " + t.getMessage());
            }
        });


    }

    private void getImageFromServer(List<Moto> motos, final Context context) {
        for (final Moto moto : motos) {


            Target target = new Target() {

                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Log.d("IMAGE", "Nao parece a cara do Beto.");
                    FileOutputStream fs = null;
                    try {
                        fs = context.openFileOutput(moto.getModelo() + ".jpg", Context.MODE_PRIVATE);

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, fs);

                        fs.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    Log.d("IMAGE", "parece a cara do Beto");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };

            String uri = (NetworkConnection.BASE_URL + "images/" + moto.getModelo() + ".jpg");
            Picasso.with(context).load(uri).into(target);

        }
    }
}
