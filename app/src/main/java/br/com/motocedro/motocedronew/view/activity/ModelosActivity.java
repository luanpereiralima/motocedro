package br.com.motocedro.motocedronew.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.List;

import br.com.motocedro.motocedronew.R;
import br.com.motocedro.motocedronew.model.Moto;
import br.com.motocedro.motocedronew.model.repositories.MotoRepository;
import br.com.motocedro.motocedronew.services.LocationListenerService;
import br.com.motocedro.motocedronew.services.Point;
import br.com.motocedro.motocedronew.services.RegistrationService;
import br.com.motocedro.motocedronew.view.adapter.RecyclerViewModelosAdapter;
import io.realm.Realm;
import io.realm.RealmResults;

public class ModelosActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;

    public static String MOTO = "com.motocedro.motocedro.MOTO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modelos_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.modelosRecycler);

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        MotoRepository motoRepository = new MotoRepository();

        List<Moto> motos = motoRepository.getMotos(this);

        RecyclerViewModelosAdapter modelosAdapter = new RecyclerViewModelosAdapter(motos);

        mRecyclerView.setAdapter(modelosAdapter);

        startService(new Intent(this, RegistrationService.class));

        startService(new Intent(this, LocationListenerService.class));

    }

    @Override
    public void onResume(){
       super.onResume();

        Realm realm = Realm.getInstance(this.getApplicationContext());

        RealmResults<Point> results = realm.where(Point.class).findAll();

        Log.i("REALM",results.size()+" qtd de poins");

        Log.i("MOTOS", "sendo resumida");
    }

}
