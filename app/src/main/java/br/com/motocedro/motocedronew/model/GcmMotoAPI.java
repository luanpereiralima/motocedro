package br.com.motocedro.motocedronew.model;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by estilon on 07/11/15.
 */
public interface GcmMotoAPI {

    @POST("/gcm/sendToken")
    public Call<GcmToken> send(@Body GcmToken gcmToken);
}
