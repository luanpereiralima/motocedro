package br.com.motocedro.motocedronew.services;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by estilon on 08/11/15.
 */
public class MyInstanceIDListenerService extends InstanceIDListenerService {

    public static String LOG = "LOG";

    @Override
    public void onTokenRefresh(){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        preferences.edit().putBoolean(RegistrationService.KEY_NAME,false);

        Intent intent = new Intent(this, RegistrationService.class);

        startService(intent);
    }


}
