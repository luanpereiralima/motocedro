package br.com.motocedro.motocedronew.model;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by estilon on 27/10/15.
 */
public interface MotoAPI {

    @GET("getOneMoto")
    Call<Moto> getOneMoto();


    @GET("getLastMotos")
    Call<List<Moto>> getLastModifiedMotos();
}

